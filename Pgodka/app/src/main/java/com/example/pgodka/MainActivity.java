package com.example.pgodka;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    @SuppressLint("WrongViewCast")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        but1 = findViewById(R.id.button1);
        city = findViewById(R.id.editText);
        tempMain = findViewById(R.id.tempMain);
        oblate = findViewById(R.id.Kak);
        feels_like = findViewById(R.id.feels_like);
        mmtemp = findViewById(R.id.minMax);
        mmtemp = findViewById(R.id.minMax);
        sdf = findViewById(R.id.button);


        pres = findViewById(R.id.pressure);
        humid = findViewById(R.id.humidity);
        visib = findViewById(R.id.visibility);
        windspeed = findViewById(R.id.WindSpeed);
        windvektor = findViewById(R.id.WindVektor);
    }

    Button sdf;
    Button but1;
    EditText city;
    TextView tempMain;
    TextView oblate;
    TextView feels_like;
    TextView mmtemp;
    TextView pres;
    TextView humid;
    TextView visib;
    TextView windspeed;
    TextView windvektor;
    TextView lat;
    TextView lon;


    public void but1(View view) {


        String url = "https://api.openweathermap.org/data/2.5/weather?q=" + city.getText().toString() + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric&lang=ru";
        JsonObjectRequest jo = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                try {
                    JSONObject main = response.getJSONObject("main");
//                    JSONObject visibility = response.getJSONObject("visibility");
                    JSONObject wind = response.getJSONObject("wind");
                    String temp = (main.getInt("temp")) + "°";
                    String feel_like = "Ощущается как " + (main.getInt("feels_like")) + "°";
                    JSONArray ja = response.getJSONArray("weather");
                    JSONObject index = ja.getJSONObject(0);
                    String descripion = index.getString("description");
                    String kak = (main.getInt("temp_min")) + "°" + " / " + (main.getInt("temp_max")) + "°";
                    String pressure = "Давление" + "\n" + main.getInt("pressure");
                    String humidility = "Влажность" + "\n" + main.getInt("humidity");
//                    String VISIBILITY = "Видимость" + "\n" + visibility.toString();
                    String windSpeed = "Скорость ветра: " + wind.getInt("speed") + " м/с";
                    String windV = "Направление ветра: " + wind.getInt("deg") + "°";


                    tempMain.setText(temp);
                    feels_like.setText(feel_like);
                    mmtemp.setText((kak));
                    oblate.setText(descripion);
                    pres.setText(pressure);
                    humid.setText(humidility);
//                    visib.setText(VISIBILITY);
                    windspeed.setText(windSpeed);
                    windvektor.setText(windV);


                } catch (JSONException e) {
                    setTitle(e.getMessage());
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                city.setText(error.getMessage());
            }
        });
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(jo);
    }


    public void Sdf(View view) {
        try {
            String url = "https://api.openweathermap.org/data/2.5/weather?lat=" + Float.valueOf(lon.getText().toString()) + "&lon=" + Float.valueOf(lat.getText().toString()) + "&appid=b1b35bba8b434a28a0be2a3e1071ae5b&units=metric&lang=ru";
            JsonObjectRequest joq = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    try {
                        JSONObject main = response.getJSONObject("main");
//                    JSONObject visibility = response.getJSONObject("visibility");
                        JSONObject wind = response.getJSONObject("wind");
                        String temp = (main.getInt("temp")) + "°";
                        String feel_like = "Ощущается как " + (main.getInt("feels_like")) + "°";
                        JSONArray ja = response.getJSONArray("weather");
                        JSONObject index = ja.getJSONObject(0);
                        String descripion = index.getString("description");
                        String kak = (main.getInt("temp_min")) + "°" + " / " + (main.getInt("temp_max")) + "°";
                        String pressure = "Давление" + "\n" + main.getInt("pressure");
                        String humidility = "Влажность" + "\n" + main.getInt("humidity");
//                    String VISIBILITY = "Видимость" + "\n" + visibility.toString();
                        String windSpeed = "Скорость ветра: " + wind.getInt("speed") + " м/с";
                        String windV = "Направление ветра: " + wind.getInt("deg") + "°";


                        tempMain.setText(temp);
                        feels_like.setText(feel_like);
                        mmtemp.setText((kak));
                        oblate.setText(descripion);
                        pres.setText(pressure);
                        humid.setText(humidility);

                        windspeed.setText(windSpeed);
                        windvektor.setText(windV);


                    } catch (JSONException e) {
                        setTitle(e.getMessage());
                    }
                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    lon.setText(error.getMessage());
                }
            });
            RequestQueue requestQueue = Volley.newRequestQueue(this);
            requestQueue.add(joq);
        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }
}












